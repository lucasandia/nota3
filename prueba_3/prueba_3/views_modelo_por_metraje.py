class casa_metraje(object):
    def __init__(self,metros_cuadrados,precio_m2):
        self.metros_cuadrados=metros_cuadrados
        self.precio_m2=precio_m2

    def get_metros_cuadrados(self):
        if self.metros_cuadrados>=20:
            return self.metros_cuadrados
        else:
            return self.metros_cuadrados*0
            
    def get_precio_m2(self):
        return self.precio_m2

    def precio_final(self):
        if self.metros_cuadrados>=20:
            return (self.precio_m2*self.metros_cuadrados)
        else:
            return(self.precio_m2*self.metros_cuadrados*0)

            
        

from django.http import HttpResponse
from django.template.loader import get_template

def funcion_casa_a_metraje(request,metros_cuadrados):
    obj=casa_metraje(metros_cuadrados,33000)
    doc_externo= get_template('pagina_casa_por_metraje.html')
    documento=doc_externo.render({"metros_cuadrados":obj.get_metros_cuadrados,"precio_final":obj.precio_final,"precio_m2":obj.get_precio_m2})
    return HttpResponse(documento)
