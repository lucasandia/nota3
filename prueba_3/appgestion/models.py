from django.db import models

# Create your models here.

class Casa(models.Model):
    nombre=models.CharField(max_length=50)
    cantidad_habitaciones=models.IntegerField()
    cantidad_m2=models.IntegerField()
    cantidad_baños=models.IntegerField()
    living_comedor=models.CharField(max_length=2)
    cocina=models.CharField(max_length=2)

class Solicitud_Contacto(models.Model):
    nombre=models.CharField(max_length=50)
    correo=models.CharField(max_length=30)
    comuna=models.CharField(max_length=30)
    telefono=models.IntegerField()
    mensaje=models.CharField(max_length=100)

